# api_vs_abi

This sample project illustrates how "safe" changes in `API` can affect `ABI` backwards compatibility.

## Java

This sample illustrates issue while dealing with "small" changes affecting `API` of Java code.

We make very simple change of one method:

    public double add(short a) {         public double add(int a) {
      return (double) (a + a);     =>      return (double) (a + a);
    }                                    }

# before you proceed any further

Make sure to go insde java directory

    cd java

# compile with library Simple - version `1.2.3`

Code is compiled with version `1.2.3` of the library. After this is done, we start code with versions: `1.2.3`, `1.3.0` and `1.3.0_broken`. As you can see, broken release will make code fail. However, we can still recompile source code - `Main` - with new library.

    find . -name "Simple.java" -exec javac {} \;
    javac -cp .:1.2.3 Main.java

# run source code - `Main` - with all the libs

As you can see it works fine for `1.2.3` and `1.3.0` but fails for `1.3.0_broken`
    
    find . -type d -name "1*" -exec echo "Testing {}" \; -exec java -cp .:{} Main \;

# compile with library Simple - version `1.3.0_broken`

This time, let's compile with version `1.3.0_broken`

    find . -name "Simple.java" -exec javac {} \;
    javac -cp .:1.3.0_broken Main.java

# run source code - `Main` - with all the libs

This time, code fails with release `1.2.3` while at the same time works fine with `1.3.0` and `1.3.0_broken`

    find . -type d -name "1*" -exec echo "Testing {}" \; -exec java -cp .:{} Main \;

Take a look at source code of `1.3.0` version and you will notice why this version is really backward compatible in terms of both: `API` and `ABI`.

# working with Make

    make compile_123
    make compile_130_broken
    make test
    make clean

